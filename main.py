import csv
import random
import time
import requests
from concurrent.futures import ThreadPoolExecutor, as_completed

from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait

service = Service(r'D:\IDE_TOOLS\Pythons\BrowserDrivers\chrome\chromedriver.exe')

# option.add_argument('--headless')
csvfile = 'test4.csv'   # 汉语的数据文件
newfile = 'result4.csv'  # 存放汉语和翻译结果的数据文件
useragent = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'
# proxy_url = 'http://v2.api.juliangip.com/dynamic/getips?filter=1&num=1&pt=1&result_type=text&split=2&trade_no=1235831351935714&sign=acdb7a9e55a38259b586aa22924ba5ff'
proxy_url = 'http://v2.api.juliangip.com/dynamic/getips?num=1&pt=1&result_type=text&split=2&trade_no=1900666036396845&sign=dbe98ea6b91807803457cbbc89bf671a'

def getPage(rows):
    try:
        proxy_ip = requests.get(proxy_url).text   # 设置代理IP
        print(proxy_ip)
        option = webdriver.ChromeOptions()
        option.add_argument(f'--proxy-server={proxy_ip}')
        option.add_argument(f'User-Agent={useragent}')   # 设置UA
        wd = webdriver.Chrome(service=service, options=option)
        wd.implicitly_wait(7)    # 隐式等待7s
        wd.get('https://www.fanyi1234.com/ti/')
        count = 0   # 计数用
        for i in range(len(rows)):   # 遍历需要翻译的内容
            try:
                transbutton = wd.find_element(By.XPATH,
                                              '//*[@id="__layout"]/div/div[1]/div/div[1]/div[1]/div[1]/div[2]')
                time.sleep(1)
                transbutton.click()
                time.sleep(0.5)
                textarea = wd.find_element(By.XPATH,
                                           '//*[@id="__layout"]/div/div[1]/div/div[1]/div[2]/div[1]/div[1]/div/div[1]')
                textarea.send_keys(rows[i][0])
                result_ele = WebDriverWait(wd, 30, 0.2).until(
                    lambda x: x.find_element(By.XPATH, '//*[@id="copyInner"]/div/div[2]')).text
                # 将获取到的翻译结果存入到 rows的列表中
                rows[i][1] = result_ele
                print(rows[i])
            except Exception as e:
                # 超时导致的翻译失败， 置为 '0' 做标记，用于修复时识别
                print(f"{rows[i][0]}翻译失败!")
                rows[i][1] = '0'
                continue
            finally:
                count += 1
                print(f"当前位置:{count}")
                time.sleep(random.uniform(1, 2))
                # 刷新页面，此法可以清除上一条的翻译结果。 如果使用 清除输入内容，上一条的翻译结果并不会被清除，进而可能会导致连续多条翻译的结果一样，
                wd.refresh()
                time.sleep(random.uniform(1, 2))
    except Exception as e:
        # 打开页面时出现错误
        print(f"翻译失败!{e}")
    finally:
        wd.close()
        return rows


def getOnePiece(row):
    # 逐条获取翻译结果，多线程情况有bug，用于单线程稳定
    # 本方法用于修复翻译失败的数据
    try:
        wd = webdriver.Chrome(service=service)
        wd.implicitly_wait(7)
        wd.get('https://www.fanyi1234.com/ti/')
        transbutton = wd.find_element(By.XPATH,
                                      '//*[@id="__layout"]/div/div[1]/div/div[1]/div[1]/div[1]/div[2]')
        time.sleep(1)
        transbutton.click()
        time.sleep(0.5)
        textarea = wd.find_element(By.XPATH,
                                   '//*[@id="__layout"]/div/div[1]/div/div[1]/div[2]/div[1]/div[1]/div/div[1]')
        textarea.send_keys(row[0])
        result_ele = WebDriverWait(wd, 30, 0.2).until(
            lambda x: x.find_element(By.XPATH, '//*[@id="copyInner"]/div/div[2]')).text
        row[1] = result_ele
        print(row)
    except Exception as e:
        print(f"翻译失败!{e}")
    finally:
        wd.close()
        return row

def upSpeed():
    # 此方法效率较低
    with open(csvfile) as of, open(newfile, 'w', encoding='utf-8', newline='') as nf:
        reader = csv.reader(of)
        writer = csv.writer(nf)
        writer.writerow(['CN', 'ZANG'])
        all_row = [row for row in reader]
        rows_list = [all_row[i:i + 3] for i in range(1, len(all_row), 3)]
        with ThreadPoolExecutor(max_workers=3) as t:
            obj_list = []
            for rows in rows_list:
                obj = t.submit(getPage, rows)
                obj_list.append(obj)
            for future in as_completed(obj_list):
                datas = future.result()
                print(datas)
                for data in datas:
                    writer.writerow(data)


def upspeed3():
    # 多线程处理数据，此方法效率较高
    # 先读取未被翻译的文件
    with open(csvfile, 'r', encoding='utf-8') as of:
        reader = csv.reader(of)
        all_row = [row for row in reader]
        l = len(all_row)
        # 将数据内容分成 n 份，此时为500
        step = int(l / 500)
        rows_list = [all_row[i:i + step] for i in range(0, l, step)]

        # 开启线程池，最大10个线程
        with ThreadPoolExecutor(max_workers=10) as t:
            # 线程任务列表
            obj_list = []
            for rows in rows_list:
                # 方法和参数提交并存入列表
                obj = t.submit(getPage, rows)
                obj_list.append(obj)
                # 等0.5s更稳定
                time.sleep(0.5)

            # 线程结束，将返回的结果存入 newfile文件中
            for future in as_completed(obj_list):
                with open(newfile, 'a', encoding='utf-8', newline='') as nf:
                    writer = csv.writer(nf)
                    datas = future.result()
                    print(datas)
                    # 逐条写入
                    for data in datas:
                        writer.writerow(data)


def sol_error():
    # 单线程处理翻译失败的数据
    # 打开 result.csv 文件，没问题的和修复后的数据 存入 new_result.csv文件
    with open('result4.csv', 'r', encoding='utf-8') as rf, open('new_result4.csv', 'a', encoding='utf-8',
                                                                newline='', buffering=1) as nrf:
        reader = csv.reader(rf)
        writer = csv.writer(nrf)
        reader = list(reader)
        for row in reader:
            # flag 标志用于解决 某条数据因网站原因或网络原因长时间翻译不出来导致死循环 的问题，重复翻译10次则进入下一条
            flag = 0
            # 翻译失败的数据 ‘’的为代理ip的问题  '0'绝大部分是网站长时间翻译不出来的问题
            while (row[1] == '' or row[1] == '0') and (flag <= 10):
                print(row)
                # 逐条解决
                row = getOnePiece(row)
                # 标志记录次数
                flag+=1
            # 没问题的和修复过的写入新的文件
            writer.writerow(row)

def combine_data():
    # 此方法用于合并数据
    # 将 new_result1 new_result2 和 new_result3 合并入 new_result1中，共 29476条
    with open('new_result1.csv', 'a', encoding='utf-8', newline='') as nrf1, open('new_result3.csv', 'r',
                                                                                  encoding='utf-8') as nrf2:
        writer = csv.writer(nrf1)
        reader = csv.reader(nrf2)
        for row in reader:
            print(row)
            writer.writerow(row)
def exam_data():
    # 此方法用于检验数据是否全部翻译完毕
    with open('new_result1.csv','r',encoding='utf-8') as nrf:
        reader = csv.reader(nrf)
        idx = 0
        for row in reader:
            if row[1]=='0' or row[1]=='':
                print(idx,row)
            idx+=1

if __name__ == '__main__':
    # upspeed3() # 用于获取翻译结果
    # sol_error()  # 用于修复翻译失败的结果
    # combine_data() # 用于合并多次获得的数据
    exam_data() # 用于检验结果